package main

import (
	"encoding/json"
	"encoding/xml"
	"fmt"
	"net/http"
)

type Customer struct {
	Name    string `json:"full_name" xml:"name"`
	City    string `json:"city" xml:"city"`
	ZipCode string `json:"zip_code" xml:"zipcode"`
}

func greet(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, "Hello World!!!")
}

func getAllCustomers(w http.ResponseWriter, r *http.Request) {
	//list of customers
	customers := []Customer{
		{Name: "Diogo", City: "Porto", ZipCode: "4515-290"},
		{Name: "David", City: "Cambridge", ZipCode: "02238"},
	}

	if r.Header.Get("Content-Type") == "application/xml" {
		//to define as a xml format
		w.Header().Add("Content-Type", "application/xml")
		//Encode to xml
		xml.NewEncoder(w).Encode(customers)
	} else {
		//to define as a json format
		w.Header().Add("Content-Type", "application/json")
		//Encode to json
		json.NewEncoder(w).Encode(customers)
	}
}
