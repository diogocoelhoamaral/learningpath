https://www.udemy.com/course/rest-based-microservices-api-development-in-go-lang/learn/lecture/22101352?start=0#overview

Prerequirements:

- First exercise https://go.dev/tour/welcome/1
- Should understand interfaces, structs and receiver functions
- Object Oriented programming Principles

Configure your env:
https://www.youtube.com/watch?v=dgIh-VYcWYw

Objetives:

- Dev environment and the right tools
- Hexagonal Architecture
- Dependency inversion in GO and working with stubs
- JWT Tokens
- Banking-Auth microservice based on OAuth Standard
- Unit Test for routes and other components using mocks and states based tests


Complete code is available at:

Banking             https://github.com/ashishjuyal/banking
Banking-Auth   https://github.com/ashishjuyal/banking-auth
Banking-lib       https://github.com/ashishjuyal/banking-lib

Database SQL
https://github.com/ashishjuyal/banking/blob/master/resources/database.sql

Database docker-compose file
https://github.com/ashishjuyal/banking/tree/master/resources/docker

Keyboard shortcuts for IntelliJ-IDEA
https://www.youtube.com/watch?v=QYO5_riePOQ&feature=youtu.be

Postman
https://www.postman.com/downloads/

cURL / cURL for windows
https://curl.haxx.se/download.html
https://curl.haxx.se/windows/

DBeaver
They also have a community edition https://dbeaver.io/download/

Go download & install
https://golang.org/doc/install

Go Tour
https://tour.golang.org/welcome/1

Notes:
